render() function from import ReactDOM module is the one responsible 
for injecting the whole React.js project to the web page.

JSX - JavaScript Syntax Extension or JavaScript XML
 - is an extension that helps describe the UI's structure
 - used to create React "elements"
 - enables us to separate concerns into "components" that both have markup and logic in them

 Activity:
 1. Create a Course component that will render the following information:
 >src > components > Course.js
 	a. Course name
 	b. Course description
 	c. Course price

bootstrap components
	card
	button

2. Render this single course component in index.js for checking