// base import
import React from 'react';

// bootstrap components
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// export Banner
export default function Banner(){
	return(
		<Row>
			<Col>
				<Jumbotron>
					<h1>Zuitt Bootcamp</h1>
					<p>Opportunities for everyone, everywhere.</p>
					<Button variant="primary">Enroll Now</Button>
				</Jumbotron>
			</Col>
		</Row>
	)
}

