// base import
import React from 'react';


// bootstrap components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

// export Highlights
export default function Highlights(){
	return(
		<Row>
			<Col cs={12} md={4}>
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacinia suscipit ullamcorper.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			{/* Col end */}
			<Col cs={12} md={4}>
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Study now</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacinia suscipit ullamcorper.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			{/* Col end */}
			<Col cs={12} md={4}>
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Hello World</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacinia suscipit ullamcorper.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}