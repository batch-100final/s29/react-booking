// base import
import React from 'react';

// bootstrap components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

// export Course
export default function Course(){
	return(
		<Row>
			<Col className='mt-5'>
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Course1</h2>
						</Card.Title>
							<h5>Description</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacinia suscipit ullamcorper.</p>
							<h5>Price</h5>
							<p>P 18,000</p>
							<Button variant="primary">Enroll Now</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}



//Maam thonies solution
/* 
	return(
		<Card>
			<Card.Body>
				<Card.Title>React JS Course</Card.Title>
				<h6>Descripton</h6>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<h6>Price</h6>
				<p>PhP 40,000</p>
				<Button variant="primary">Enroll Now</Button>
			</Card.Body>
		</Card>
	)
*/