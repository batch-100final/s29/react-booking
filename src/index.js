// base imports
import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App components
import AppNavBar from 'components/AppNavBar';

//Page Components
import Home from 'pages/Home';

ReactDOM.render(
	<Fragment>
		<AppNavBar />
		<Home />
	</Fragment>,
document.getElementById('root'));