// base imports
import React from 'react';

// bootstrap components
import Container from 'react-bootstrap/Container';

// app components
import Banner from 'components/Banner';
import Highlights from 'components/Highlights';
import Course from 'components/Course';

export default function Home(){
	return(
		<Container fluid>
			<Banner />
			<Container>
				<Highlights />
				<Course />
			</Container>
		</Container>
	)
}